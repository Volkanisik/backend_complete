# Complete Backend with JWT,Bcrypt,Exceptions,Hybernate :rocket:

### Database Mysql
#### `docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=yourpassword -d mysql`

Run the commands down below at database:
```sql
create database db_example; -- Creates the new database
create user 'springuser'@'%' identified by 'ThePassword'; -- Creates the user
grant all on db_example.* to 'springuser'@'%'; -- Gives all privileges to the new user on the newly created database
```
