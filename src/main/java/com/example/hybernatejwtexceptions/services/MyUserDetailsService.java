package com.example.hybernatejwtexceptions.services;


import com.example.hybernatejwtexceptions.data.User;
import com.example.hybernatejwtexceptions.data.UserRepository;
import com.example.hybernatejwtexceptions.exceptions.ResourceNotFoundException;
import com.example.hybernatejwtexceptions.Authentication.util.BcryptPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Lazy;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {
    private UserRepository userRepository;
    private BcryptPasswordEncoder bcryptPasswordEncoder;

    @Autowired
    public MyUserDetailsService(UserRepository userRepository,
                       @Lazy BcryptPasswordEncoder bcryptPasswordEncoder){
        this.userRepository = userRepository;
        this.bcryptPasswordEncoder = bcryptPasswordEncoder;

    }

    public void registerUser(User user) {
        User newUser = new User();
        newUser.setId(user.getId());
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setEmail(user.getEmail());
        newUser.setUsername(user.getUsername());
        newUser.setPassword(bcryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(newUser);
    }

    public User findUserById(int id) {

        Optional<User> optional = userRepository.findById(id);
        if(optional.isPresent()){
            return optional.get();
        }else{
            throw new ResourceNotFoundException("User not found " + id);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<User> users = userRepository.findAll();

        for (User user : users) {
            if(user.getUsername().equals(username)){
                org.springframework.security.core.userdetails.User secuser = new org.springframework.security.core.userdetails.User(user.getUsername(),
                        user.getPassword(), Collections.emptyList());
                return secuser;
            }
        }
        throw new UsernameNotFoundException("user with username: " + username + " not found.");
    }
}
