package com.example.hybernatejwtexceptions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HybernateJwtExceptionsApplication {

    public static void main(String[] args) {
        SpringApplication.run(HybernateJwtExceptionsApplication.class, args);
    }

}
