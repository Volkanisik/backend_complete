package com.example.hybernatejwtexceptions.data;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity // This tells Hibernate to make a table out of this class
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String password;

    private String firstName;

    private String lastName;

    private String username;

    private String email;

}
